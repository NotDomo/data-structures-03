package laborai.Lab3Lukosevicius;

import laborai.studijosktu.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


public class MapKTUOA<K, V> implements MapADT<K, V>, MapADTx<K, V> {

    public static final int DEFAULT_INITIAL_CAPACITY = 16;
    public static final float DEFAULT_LOAD_FACTOR = 0.75f;
    public static final HashType DEFAULT_HASH_TYPE = HashType.JCF8;

    // Maišos lentelė
    protected Node<K, V>[] table;
    // Lentelėje esančių raktas-reikšmė porų kiekis
    protected int size = 0;
    // Apkrovimo faktorius
    protected float loadFactor;
    // Maišos metodas
    protected HashType ht;
    //--------------------------------------------------------------------------
    //  Maišos lentelės įvertinimo parametrai
    //--------------------------------------------------------------------------
    // Maksimalus suformuotos maišos lentelės grandinėlės ilgis
    protected int maxChainSize = 1;
    // Permaišymų kiekis
    protected int rehashesCounter = 0;
    // Paskutinės patalpintos poros grandinėlės indeksas maišos lentelėje
    protected int lastUpdatedChain = 0;
    // Lentelės grandinėlių skaičius
    protected int chainsCounter = 0;
    // Einamas poros indeksas maišos lentelėje
    protected int index = 0;
    
    public MapKTUOA() {
        this(DEFAULT_HASH_TYPE);
    }

    public MapKTUOA(HashType ht) {
        this(DEFAULT_INITIAL_CAPACITY, ht);
    }

    public MapKTUOA(int initialCapacity, HashType ht) {
        this(initialCapacity, DEFAULT_LOAD_FACTOR, ht);
    }

    public MapKTUOA(float loadFactor, HashType ht) {
        this(DEFAULT_INITIAL_CAPACITY, loadFactor, ht);
    }

    public MapKTUOA(int initialCapacity, float loadFactor, HashType ht) {
        if (initialCapacity <= 0) {
            throw new IllegalArgumentException("Illegal initial capacity: " + initialCapacity);
        }

        if ((loadFactor <= 0.0) || (loadFactor > 1.0)) {
            throw new IllegalArgumentException("Illegal load factor: " + loadFactor);
        }

        this.table = new Node[initialCapacity];
        this.loadFactor = loadFactor;
        this.ht = ht;
    }
    
    public MapKTUOA(K baseKey, V baseObj, int initialCapacity, float loadFactor, HashType ht) {
        this(initialCapacity, loadFactor, ht);
    }
    
    @Override
    public boolean isEmpty() {
        return size == 0;
    }

    @Override
    public int size() {
        return size;
    }
    

    @Override
    public void clear() {
        Arrays.fill(table, null);
        size = 0;
        index = 0;
        lastUpdatedChain = 0;
        maxChainSize = 0;
        rehashesCounter = 0;
        chainsCounter = 0;
    }
    
    @Override
    public boolean contains(K key) {
        return get(key) != null;
    }

    @Override
    public String[][] toArray() {
        String[][] array = new String[size][1];
        int index = 0;
        for (Node node : table) {
            if (node != null)
                array[index++][0] = node.toString();
        }
        return array;
    }
    
    private int findPosition(K key){
        int index = hash(key, ht);
        int index0 = index;
        for (int j = 0, i = 0; j < table.length; j++, i++) {
            if(table[index] == null || table[index].key.equals(key))
            {
                return index;
            }
            i++;
            index = (index0 + i * hash2(key)) % table.length;
        }
        return -1;
    }
    
    private int hash2(K key){
        return 7 - (Math.abs(key.hashCode()) % 7);
    }

    @Override
    public V put(K key, V value) {
        if (key == null || value == null) {
            throw new IllegalArgumentException("Key or value is null "
                    + "in put(Key key, Value value)");
        }
        index = findPosition(key);
        while(index == -1){ //Jei tuscio elemento neranda
            rehash();
            index = findPosition(key);
        }
        Node<K, V> n = new Node(key, value);
        table[index] = n;
        size++;
        return value; 
    }
    
    private int hash(K key, HashType ht) {
        int h = key.hashCode();
        switch (ht) {
            case DIVISION:
                return Math.abs(h) % table.length;
            case MULTIPLICATION:
                double k = (Math.sqrt(5) - 1) / 2;
                return (int) (((k * Math.abs(h)) % 1) * table.length);
            case JCF7:
                h ^= (h >>> 20) ^ (h >>> 12);
                h = h ^ (h >>> 7) ^ (h >>> 4);
                return h & (table.length - 1);
            case JCF8:
                h = h ^ (h >>> 16);
                return h & (table.length - 1);
            default:
                return Math.abs(h) % table.length;
        }
    }
    
    private void rehash() {
        MapKTUOA map = new MapKTUOA(table.length * 2, loadFactor, ht);
        for (Node<K, V> table1 : table) {
            if (table1 != null) {
                map.put(table1.key, table1.value);
            }
        }
        table = map.table;
        rehashesCounter++;
    }

    @Override
    public V get(K key) {
        if (key == null) throw new IllegalArgumentException("Key is null in get(Key key)");
        index = findPosition(key);
        if (index == -1)
            return null;
        Node<K, V> n = table[index];
        if(n != null){
            return n.value;
        }
        return null;    
    }

     @Override
    public V remove(K key) {
        if (key == null) throw new IllegalArgumentException("Key is null in remove(Key key)");
        index = findPosition(key);
        if(index == -1) return null;
        Node<K, V> n = table[index];
        if(n == null) return null;
        V temp = n.value;
        n.value = null;
        n.key = null;
        table[index] = null;
        size--;
        return temp;
    }

    @Override
    public String toString() {
        StringBuilder result = new StringBuilder();
        for (Node<K, V> node : table) {
            if (node != null) {
                for (Node<K, V> n = node; n != null; n = n.next) {
                    result.append(n.toString()).append(System.lineSeparator());
                }
            }
        }
        return result.toString();
    }
    
    @Override
    public V put(String dataString) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void load(String filePath) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void save(String filePath) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void println() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void println(String title) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public String[][] getModelList(String delimiter) {
        String[][] result = new String[table.length][];
        int count = 0;
        for (Node<K, V> n : table) {
            List<String> list = new ArrayList();
            list.add( "[ " + count + " ]");
            while (n != null) {
                list.add("-->");
                list.add(split(n.toString(), delimiter));
                n = n.next;
            }
            result[count] = list.toArray(new String[0]);
            count++;
        }
        return result;
    }

    private String split(String s, String delimiter) {
        int k = s.indexOf(delimiter);
        if (k <= 0) {
            return s;
        }
        return s.substring(0, k);
    }
    
    @Override
    public int getMaxChainSize() {
        return maxChainSize;
    }

    @Override
    public int getRehashesCounter() {
        return rehashesCounter;
    }

    @Override
    public int getTableCapacity() {
         return table.length;
    }

    @Override
    public int getLastUpdatedChain() {
         return index;
    }

    @Override
    public int getChainsCounter() {
       return 0;
    }

    protected class Node<K, V> {
        K key;
        V value;
        Node<K, V> next;
        Node(K key, V value, Node<K, V> next)
        {
            this.key = key;
            this.value = value;
            this.next = next;
        }

        Node(K key, V value){
            this.key = key;
            this.value = value;
            this.next = null;
        }

        Node() {}


        @Override
        public String toString() {
            return key + "=" + value;
        }
    }
    
}
