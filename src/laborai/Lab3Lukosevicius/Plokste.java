package laborai.Lab3Lukosevicius;

import laborai.studijosktu.*;

import java.util.InputMismatchException;
import java.util.NoSuchElementException;
import java.util.Objects;
import java.util.Random;
import java.util.Scanner;

public final class Plokste implements KTUable {

    private static final int priimtinosAtmintiesRiba = 1;
    private static final double minKaina = 100.0;
    private static final double maxKaina = 333000.0;
    private String gamintojas = ""; // marke
    private String modelis = "";
    private int atmintiesKiekis = -1; // gamMetai
    private int palaikomiMonitoriai = -1; // rida
    private double kaina = -1.0;

    public Plokste() {
    }

    public Plokste(String gamintojas, String modelis,
            int atmintiesKiekis, int palaikomiMonitoriai, double kaina) {
        this.gamintojas = gamintojas;
        this.modelis = modelis;
        this.atmintiesKiekis = atmintiesKiekis;
        this.palaikomiMonitoriai = palaikomiMonitoriai;
        this.kaina = kaina;
        validate();
    }

    public Plokste(String e) {
        this.parse(e);
    }

    public Plokste(Builder builder) {
        this.gamintojas = builder.gamintojas;
        this.modelis = builder.modelis;
        this.atmintiesKiekis = builder.atmintiesKiekis;
        this.palaikomiMonitoriai = builder.palaikomiMonitoriai;
        this.kaina = builder.kaina;
        validate();
    }

    @Override
    public Plokste create(String dataString) {
        return new Plokste(dataString);
    }

    @Override
    public String validate() {
        String klaidosTipas = "";
        assert (atmintiesKiekis > priimtinosAtmintiesRiba) :
                klaidosTipas = "Blogai nurodyta atmintis; ";
        assert (kaina < minKaina || kaina > maxKaina) :
                klaidosTipas += "Kaina už leistinų ribų; ";
        return klaidosTipas;
    }

    @Override
    public void parse(String dataString) {
        try { 
            Scanner ed = new Scanner(dataString);
            gamintojas = ed.next();
            modelis = ed.next();
            atmintiesKiekis = ed.nextInt();
            setPalaikomiMonitoriai(ed.nextInt());
            setKaina(ed.nextDouble());
            validate();
        } catch (InputMismatchException e) {
            Ks.ern("Blogas duomenų formatas apie plokste -> " + dataString);
        } catch (NoSuchElementException e) {
            Ks.ern("Trūksta duomenų apie plokste -> " + dataString);
        }
    }

    @Override
    public String toString() {
        return gamintojas + "_" + modelis + ":" + atmintiesKiekis + " " + getPalaikomiMonitoriai() + " "
                + String.format("%4.1f", kaina);
    }

    public String getGamintojas() { //getMarke
        return gamintojas;
    }

    public String getModelis() {
        return modelis;
    }

    public int getAtmintiesKiekis() { //gamMetai
        return atmintiesKiekis;
    }

    public int getPalaikomiMonitoriai() { //getRida
        return palaikomiMonitoriai;
    }

    public double getKaina() {
        return kaina;
    }

    public void setKaina(double kaina) {
        this.kaina = kaina;
    }

    public void setPalaikomiMonitoriai(int palaikomiMonitoriai) {
        this.palaikomiMonitoriai = palaikomiMonitoriai;
    }

    @Override
    public int hashCode() {
        return Objects.hash(gamintojas, modelis, atmintiesKiekis, palaikomiMonitoriai, kaina);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Plokste other = (Plokste) obj;
        if (!Objects.equals(this.gamintojas, other.gamintojas)) {
            return false;
        }
        if (!Objects.equals(this.modelis, other.modelis)) {
            return false;
        }
        if (this.atmintiesKiekis != other.atmintiesKiekis) {
            return false;
        }
        if (this.palaikomiMonitoriai != other.palaikomiMonitoriai) {
            return false;
        }
        if (Double.doubleToLongBits(this.kaina) != Double.doubleToLongBits(other.kaina)) {
            return false;
        }

        return true;
    }

    // Ploksciu klases objektų gamintojas
     public static class Builder {

        private final static Random RANDOM = new Random(1949);  // Atsitiktinių generatorius
        private final static String[][] MODELIAI = {
            {"AMD", "PRO WX 4100", "PRO SSG"},
            {"Gigabyte", "GeForce GTX 1050 TI D5"},
            {"EVGA", "GeForce GTX 1070 SC", "GeForce GTX 1060"},
            {"Sapphire", "RADEON RX 580"},
            {"Fujitsu", "NVIDIA Quadro P4000"},
        };

        private String gamintojas = "";
        private String modelis = "";
        private int atmintiesKiekis = 0;  
        private int palaikomiMonitoriai = 0;
        private double kaina = 0.0; 

        public Plokste build() {
            return new Plokste(this);
        }

        public Plokste buildRandom() {
            int ma = RANDOM.nextInt(MODELIAI.length);        // gamintojo indeksas  0..
            int mo = RANDOM.nextInt(MODELIAI[ma].length - 1) + 1;// modelio indeksas 1..              
            return new Plokste(MODELIAI[ma][0],
                    MODELIAI[ma][mo],
                    1 + RANDOM.nextInt(16),// atminties kiekis tarp 1 ir 16
                    1 + RANDOM.nextInt(6),// palaikomi monitoriai tarp 1 ir 6
                    200 + RANDOM.nextDouble() * 1500);// kaina tarp 200 ir 1500
        }

        public Builder atmintiesKiekis(int atmintiesKiekis) {
            this.atmintiesKiekis = atmintiesKiekis;
            return this;
        }

        public Builder gamintojas(String gamintojas) {
            this.gamintojas = gamintojas;
            return this;
        }

        public Builder modelis(String modelis) {
            this.modelis = modelis;
            return this;
        }

        public Builder palaikomiMonitoriai(int palaikomiMonitoriai) {
            this.palaikomiMonitoriai = palaikomiMonitoriai;
            return this;
        }

        public Builder kaina(double kaina) {
            this.kaina = kaina;
            return this;
        }
    }
}
