package laborai.Lab3Lukosevicius;

import laborai.gui.MyException;
import java.util.Arrays;
import java.util.Collections;
import java.util.stream.IntStream;

public class PloksciuGamyba {

    private static final String ID_CODE = "TA";      //  ***** nauja
    private static int serNr = 10000;               //  ***** nauja

    private Plokste[] plokstes;
    private String[] raktai;
    private int kiekis = 0, idKiekis = 0;

    public static Plokste[] gamintiPlokstes(int kiekis) {
        Plokste[] plokstes = IntStream.range(0, kiekis)
                .mapToObj(i -> new Plokste.Builder().buildRandom())
                .toArray(Plokste[]::new);
        Collections.shuffle(Arrays.asList(plokstes));
        return plokstes;
    }

    public static String[] gamintiPlokIds(int kiekis) {
        String[] raktai = IntStream.range(0, kiekis)
                .mapToObj(i -> ID_CODE + (serNr++))
                .toArray(String[]::new);
        Collections.shuffle(Arrays.asList(raktai));
        return raktai;
    }

    public Plokste[] gamintiIrParduotiPlokstes(int aibesDydis,
                                                int aibesImtis) throws MyException {
        if (aibesImtis > aibesDydis) {
            aibesImtis = aibesDydis;
        }
        plokstes = gamintiPlokstes(aibesDydis);
        raktai = gamintiPlokIds(aibesDydis);
        this.kiekis = aibesImtis;
        return Arrays.copyOf(plokstes, aibesImtis);
    }

    // Imamas po vienas elementas iš sugeneruoto masyvo. Kai elementai baigiasi sugeneruojama
    // nuosava situacija ir išmetamas pranešimas.
    public Plokste parduotiPlokste() {
        if (plokstes == null) {
            throw new MyException("carsNotGenerated");
        }
        if (kiekis < plokstes.length) {
            return plokstes[kiekis++];
        } else {
            throw new MyException("allSetStoredToMap");
        }
    }

    public String gautiIsBazesPlokId() {
        if (raktai == null) {
            throw new MyException("carsIdsNotGenerated");
        }
        if (idKiekis >= raktai.length) {
            idKiekis = 0;
        }
        return raktai[idKiekis++];
    }
}