package laborai.Lab3Lukosevicius;

import laborai.studijosktu.*;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashMap;
import laborai.gui.MyException;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.ResourceBundle;
import java.util.Scanner;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Semaphore;
import java.util.concurrent.SynchronousQueue;

public class GreitaveikosTyrimas {

    public static final String FINISH_COMMAND = "finishCommand";
    private static final ResourceBundle MESSAGES = ResourceBundle.getBundle("laborai.gui.messages");

    private final BlockingQueue resultsLogger = new SynchronousQueue();
    private final Semaphore semaphore = new Semaphore(-1);
    private final Timekeeper tk;

    private final String[] TYRIMU_VARDAI = {"MapKTU put", "HashSet put", "MapKTU contains", "HashSet contains",
    "add0.75", "add0.25", "rem0.75", "rem0.25", "get0.75", "get0.25"};
    private final int[] TIRIAMI_KIEKIAI = {2000, 4000, 8000, 16000};

    private final MapKTU<String, String> plokAtvaizdis
            = new MapKTU();
    private final HashMap<String, String> plokAtvaizdis2
            = new HashMap<>();
    private final Queue<String> chainsSizes = new LinkedList<>();
    private final List<String> zod = new ArrayList();
    public GreitaveikosTyrimas() {
        semaphore.release();
        tk = new Timekeeper(TIRIAMI_KIEKIAI, resultsLogger, semaphore);
    }

    public void pradetiTyrima() {
        try {
            SisteminisTyrimas();
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        } catch (Exception ex) {
            ex.printStackTrace(System.out);
        }
    }

    
    public void skaityti() throws FileNotFoundException{
        File failas = new File("C:\\Users\\domluk\\Desktop\\zodynas.txt");
        Scanner scanner = new Scanner(failas); 
        while (scanner.hasNextLine()) zod.add(scanner.nextLine()); 
    }

    public void SisteminisTyrimas() throws InterruptedException {
        try {
            //chainsSizes.add(MESSAGES.getString("msg4"));
            //chainsSizes.add("   kiekis      " + TYRIMU_VARDAI[0] + "   " + TYRIMU_VARDAI[1]);
            for (int k : TIRIAMI_KIEKIAI) {
                String[] plokArray = PloksciuGamyba.gamintiPlokIds(k);
                String[] plokIdArray = PloksciuGamyba.gamintiPlokIds(k);
                plokAtvaizdis.clear();
                plokAtvaizdis2.clear();
                tk.startAfterPause();
                tk.start();
        
                int i = 0;
                for (String s : plokArray) {
                    plokAtvaizdis.put(plokIdArray[i], s);
                    i++;
                }
                tk.finish(TYRIMU_VARDAI[0]);
                i = 0;
                for (String s : plokArray) {
                    plokAtvaizdis2.put(plokIdArray[i], s);
                    i++;
                }
                tk.finish(TYRIMU_VARDAI[1]);

                for (String s : plokIdArray) {
                    plokAtvaizdis.containsValue(s);
                }
                tk.finish(TYRIMU_VARDAI[2]);

                for (String s : plokIdArray) {
                    plokAtvaizdis2.containsValue(s);
                }
                tk.finish(TYRIMU_VARDAI[3]);
                tk.seriesFinish();
            }

            StringBuilder sb = new StringBuilder();
            chainsSizes.stream().forEach(p -> sb.append(p).append(System.lineSeparator()));
            tk.logResult(sb.toString());
            tk.logResult(FINISH_COMMAND);
        } catch (MyException e) {
            tk.logResult(e.getMessage());
        }
    }
    
    public void SisteminisTyrimas2() throws InterruptedException {
        try {
            chainsSizes.add(MESSAGES.getString("msg4"));
            chainsSizes.add("   kiekis      " + TYRIMU_VARDAI[4] + "   " + TYRIMU_VARDAI[5]);
            for (int k : TIRIAMI_KIEKIAI) {
                Plokste[] plokArray = PloksciuGamyba.gamintiPlokstes(k);
                String[] plokIdArray = PloksciuGamyba.gamintiPlokIds(k);
                plokAtvaizdis.clear();
                plokAtvaizdis2.clear();
                tk.startAfterPause();
                tk.start();

                for (int i = 0; i < k; i++) {
                    plokAtvaizdis.put(plokIdArray[i], plokIdArray[i]);
                    plokAtvaizdis2.put(plokIdArray[i], plokIdArray[i]);
                }
                tk.startAfterPause();
                tk.start();
                for (String s : plokIdArray) {
                    plokAtvaizdis.remove(s);
                }
                tk.finish(TYRIMU_VARDAI[6]);

                for (String s : plokIdArray) {
                    plokAtvaizdis2.remove(s);
                }
                tk.finish(TYRIMU_VARDAI[7]);

                for (String s : plokIdArray) {
                    plokAtvaizdis.get(s);
                }
                tk.finish(TYRIMU_VARDAI[8]);

                for (String s : plokIdArray) {
                    plokAtvaizdis2.get(s);
                }
                tk.finish(TYRIMU_VARDAI[9]);
                tk.seriesFinish();
            }

            StringBuilder sb = new StringBuilder();
            chainsSizes.stream().forEach(p -> sb.append(p).append(System.lineSeparator()));
            tk.logResult(sb.toString());
            tk.logResult(FINISH_COMMAND);
        } catch (MyException e) {
            tk.logResult(e.getMessage());
        }
    }

    public BlockingQueue<String> getResultsLogger() {
        return resultsLogger;
    }

    public Semaphore getSemaphore() {
        return semaphore;
    }
 
}


