package laborai.Lab3Lukosevicius;

import laborai.studijosktu.*;

import java.util.Locale;

public class PloksciuTestai {

    public static void main(String[] args) {
        Locale.setDefault(Locale.US); // suvienodiname skaičių formatus
        atvaizdzioTestas();
        //greitaveikosTestas();
    }

    public static void atvaizdzioTestas() {
        Plokste a1 = new Plokste("AMD","PRO WX 4100", 4, 4, 308);
        Plokste a2 = new Plokste("AMD PRO_SSG 16 6 5070");
        Plokste a3 = new Plokste("Gigabyte GeForce_GTX_1050_TI_D5 4 3 173");
        Plokste a4 = new Plokste("Gigabyte GeForce_GTX_1050_TI 4 2 256");
        Plokste a5 = new Plokste.Builder().buildRandom();
        Plokste a6 = new Plokste("EVGA GeForce_GTX_1070_SC 8 4 528");
        Plokste a7 = new Plokste("Fujitsu NVIDIA_Quadro P4000 8 5 600");

        // Raktų masyvas
        String[] plokId = {"TA156", "TA102", "TA178", "TA171", "TA105", "TA106", "TA107", "TA108"};
        int id = 0;
        MapKTUx<String, Plokste> atvaizdis
                = new MapKTUx(new String(), new Plokste(), HashType.DIVISION);
        // Reikšmių masyvas
        Plokste[] plok = {a1, a2, a3, a4, a5, a6, a7};
        for (Plokste a : plok) {
            atvaizdis.put(plokId[id++], a);
        }
        atvaizdis.println("Porų išsidėstymas atvaizdyje pagal raktus");
        Ks.oun("Ar egzistuoja pora atvaizdyje?");
        Ks.oun(atvaizdis.contains(plokId[6]));
        Ks.oun(atvaizdis.contains(plokId[7]));
        Ks.oun("Pašalinamos poros iš atvaizdžio:");
        Ks.oun(atvaizdis.remove(plokId[1]));
        Ks.oun(atvaizdis.remove(plokId[7]));
        atvaizdis.println("Porų išsidėstymas atvaizdyje pagal raktus");
        Ks.oun("Atliekame porų paiešką atvaizdyje:");
        Ks.oun(atvaizdis.get(plokId[2]));
        Ks.oun(atvaizdis.get(plokId[7]));
        Ks.oun("Išspausdiname atvaizdžio poras String eilute:");   
        Ks.ounn(atvaizdis);
        Ks.oun("containsValue metodas:");
        Ks.oun(atvaizdis.containsValue(a3));
        Ks.oun("putIfAbsent metodas:");
        Ks.oun(atvaizdis.putIfAbsent(plokId[7], a3));
        atvaizdis.println("Porų išsidėstymas atvaizdyje pagal raktus");
        Ks.oun("numberOfEmpties metodas:");
        Ks.oun(atvaizdis.numberOfEmpties());
    }

    //Konsoliniame režime
    private static void greitaveikosTestas() {
        System.out.println("Greitaveikos tyrimas:\n");
        GreitaveikosTyrimas gt = new GreitaveikosTyrimas();
        //Šioje gijoje atliekamas greitaveikos tyrimas
        new Thread(() -> gt.pradetiTyrima(),
                "Greitaveikos_tyrimo_gija").start();
        try {
            String result;
            while (!(result = gt.getResultsLogger().take())
                    .equals(GreitaveikosTyrimas.FINISH_COMMAND)) {
                System.out.println(result);
                gt.getSemaphore().release();
            }
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        }
        gt.getSemaphore().release();
    }
}